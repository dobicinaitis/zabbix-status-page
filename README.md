Simple status page based on data from Zabbix [Service monitoring](https://www.zabbix.com/documentation/current/manual/it_services) functionality.
<br>
Displays status of selected parent services and their first level child services.

## Screenshots
**All services operational**
<br>
![<img src="/examples/0.png" width="425" height="425">](/examples/0.png)
<br>
Expanded view
<br>
![<img src="/examples/1.png" width="425" height="425">](/examples/1.png)
<br>
Zabbix severity: Not classified, Information, Warning
<br>
<br>
![<img src="/examples/2.png" width="425" height="425">](/examples/2.png)
<br>
<br>
**Some services have problems**
<br>
![<img src="/examples/3.png" width="425" height="425">](/examples/3.png)
<br>
Zabbix severity: Average
<br>
<br>
![<img src="/examples/4.png" width="425" height="425">](/examples/4.png)
<br>
<br>
**Some services are unavailable**
<br>
![<img src="/examples/5.png" width="425" height="425">](/examples/5.png)
<br>
Zabbix severity: High, Disaster
<br>
<br>
![<img src="/examples/6.png" width="425" height="425">](/examples/6.png)
<br>

## Status mappings
| **Zabbix severity** | **Service status** | **General header info**       |
|---------------------|--------------------|-------------------------------|
| Not classified      | Operational        | All services operational      |
| Information         | Operational        | All services operational      |
| Warning             | Operational        | All services operational      |
| Average             | Has issues         | Some services have problems   |
| High                | Unavailable        | Some services are unavailable |
| Disaster            | Unavailable        | Some services are unavailable |

## Getting Started
First create an API user in Zabbix and grant it access to all hosts that have triggers on your services.
<br>
You will also need Java 11 or later to build and run the application.

### Get the code
```bash
git clone https://gitlab.com/dobicinaitis/zabbix-status-page.git
```
### Customize your settings
```bash
cd zabbix-status-page
cp src/main/resources/application.properties zabbix-status-page.properties
vi zabbix-status-page.properties
```
**Example**
```bash
# Server settings
# server.port=8080
# shows Zabbix requests/responses
logging.level.dev.dobicinaitis=DEBUG

# Zabbix settings
zabbix.api.url=https://ZABBIX_HOST/api_jsonrpc.php
zabbix.user=USER
zabbix.password=PASSWORD
# comma separated list of service IDs to use as parent services
zabbix.services=1,5,3
# cron expression specifying intervals for pooling new data from Zabbix
# second, minute, hour, day of month, month, day(s) of week
zabbix.service.update.cron.expression=0 * * * * *

# Site settings
site.title=IT Services
site.header=IT Service status page
# add a link to your support site to enable button for reporting new issues or
# comment it out if the feature is not needed
site.support.url=https://SUPPORT_HOST
site.date.format=yyyy.MM.dd HH:mm
# refresh page every X seconds
site.refresh.interval=60
```
### Build
```bash
./gradlew clean build
```
### Run 
```bash
java -jar build/libs/zabbix-status-page-*.jar --spring.config.location=./zabbix-status-page.properties
```
Navigate to http://localhost:8080 and check out your new status page 🙂