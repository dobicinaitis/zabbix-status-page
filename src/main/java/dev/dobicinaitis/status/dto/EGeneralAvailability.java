package dev.dobicinaitis.status.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/*
 * Color names/codes: https://materializecss.com/color.html
 */
@Getter
@AllArgsConstructor
public enum EGeneralAvailability {
    OK("All services operational","green"),
    PARTIAL("Some services have problems", "amber darken-2"),
    NOK("Some services are unavailable","red");

    private final String message;
    private final String color;
}
