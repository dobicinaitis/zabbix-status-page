package dev.dobicinaitis.status.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/*
* Icon names: https://material.io/resources/icons/
* Color names/codes: https://materializecss.com/color.html
* */
@Getter
@AllArgsConstructor
public enum EServiceStatus {
    OK("Operational"
            ,"0"
            ,"filter_drama"
            ,"check"
            ,"#4caf50"),
    INFORMATION("Operational"
            ,"1"
            ,"filter_drama"
            ,"check"
            ,"#4caf50"),
    WARNING("Operational"
            ,"2"
            ,"filter_drama"
            ,"check"
            ,"#4caf50"),
    AVERAGE("Has issues"
            ,"3"
            ,"bug_report"
            ,"warning"
            ,"#ffa000"),
    HIGH("Unavailable"
            ,"4"
            ,"bug_report"
            ,"error_outline"
            ,"#f44336"),
    DISASTER("Unavailable"
            ,"5"
            ,"bug_report"
            ,"error_outline"
            ,"#f44336");

    private final String statusName;
    private final String statusCode;
    private final String serviceIconName;
    private final String statusIconName;
    private final String color;
}
