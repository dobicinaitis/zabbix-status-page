package dev.dobicinaitis.status.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ZabbixServiceDTO {
    @JsonProperty("serviceid")
    private String serviceId;
    private String name;
    private String status;
    private List<ZabbixServiceDependencyDTO> dependencies;
}
