package dev.dobicinaitis.status.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;

/* Zabbix API authorization request
 * Documentation: https://www.zabbix.com/documentation/4.0/manual/api
 * Example:
 * {
 *     "jsonrpc": "2.0",
 *     "method": "user.login",
 *     "params": {
 *         "user": "Admin",
 *         "password": "zabbix"
 *     },
 *     "id": 1,
 *     "auth": null
 * }
 */
@Value
public class ZabbixAuthRequestDTO {

    private String jsonrpc = "2.0";
    private String method = "user.login";
    private Params params;
    private int id;
    private String auth = null;

    public ZabbixAuthRequestDTO(String user, String password, int id) {
        params = new Params(user,password);
        this.id = id;
    }

    @AllArgsConstructor
    public static class Params {
        @JsonProperty("user")
        private String user;
        @JsonProperty("password")
        private String password;
    }
}
