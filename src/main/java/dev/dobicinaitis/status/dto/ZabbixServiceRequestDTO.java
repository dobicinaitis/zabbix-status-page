package dev.dobicinaitis.status.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.Value;

/*
 * Retrieve Zabbix services
 * https://www.zabbix.com/documentation/3.4/manual/api/reference/service/get
 * Example:
 * {
 *     "jsonrpc": "2.0",
 *     "method": "service.get",
 *     "params": {
 *         "output": ["name","status"],
 *         "selectDependencies": "extend",
 *         "serviceids": ["2"],
 *         "limit": 100
 *     },
 *     "auth": "038e1d7b1735c6a5436ee9eae095879e",
 *     "id": 1
 * }
 */
@Value
public class ZabbixServiceRequestDTO {

    private String jsonrpc = "2.0";
    private String method = "service.get";
    private Params params;
    private String auth;
    private int id;

    public ZabbixServiceRequestDTO(String auth, int id, String[] parentIds) {
        this.auth = auth;
        this.id = id;
        this.params = new Params(parentIds);
    }

    @NoArgsConstructor
    public static class Params {
        @JsonProperty("selectDependencies")
        private String selectDependencies = "extend";
        @JsonProperty("output")
        private String[] output = {"name","status"};
        @JsonProperty("serviceids")
        private String[] serviceIds;
        @JsonProperty("sortfield")
        private String sortfield = "sortorder";

        public Params(String[] serviceIds){
            this.serviceIds = serviceIds;
        }
    }
}
