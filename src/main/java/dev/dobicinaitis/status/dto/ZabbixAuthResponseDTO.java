package dev.dobicinaitis.status.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/* Zabbix API authorization response
 * Documentation: https://www.zabbix.com/documentation/4.0/manual/api
 * Example:
 * {
 *     "jsonrpc": "2.0",
 *     "result": "0424bd59b807674191e7d77572075f33",
 *     "id": 1
 * }
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ZabbixAuthResponseDTO {
    private String result;
}
