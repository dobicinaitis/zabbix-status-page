package dev.dobicinaitis.status.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/* Example:
 * {
 *     "jsonrpc": "2.0",
 *     "result": [
 *         {
 *             "serviceid": "2",
 *             "name": "Service 1",
 *             "status": "0",
 *             "dependencies": [
 *                 {
 *                     "linkid": "1",
 *                     "serviceupid": "2",
 *                     "servicedownid": "4",
 *                     "soft": "0",
 *                     "sortorder": "0",
 *                     "serviceid": "4"
 *                 }
 *             ]
 *         },
 *         {
 *             "serviceid": "3",
 *             "name": "Service 2",
 *             "status": "0",
 *             "dependencies": []
 *         }
 *     ],
 *     "id": 2
 * }
*/
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ZabbixServiceResponseDTO {
    @JsonProperty("result")
    private List<ZabbixServiceDTO> services;
}
