package dev.dobicinaitis.status.dto;

import lombok.Value;

@Value
public class ServiceDTO {
    private String name;
    private EServiceStatus status;
}
