package dev.dobicinaitis.status;

import dev.dobicinaitis.status.dto.EGeneralAvailability;
import dev.dobicinaitis.status.dto.ServiceDTO;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @Value("${site.title}")
    private String title;
    @Value("${site.header}")
    private String header;
    @Value("${site.refresh.interval:60}")
    private String pageRefreshInterval;
    @Value("${site.support.url:}")
    private String supportUrl;
    private EGeneralAvailability generalAvailability;
    private String lastUpdated;
    private Map<ServiceDTO, List<ServiceDTO>> services;

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        lastUpdated = ZabbixServices.getLastUpdated();
        services = ZabbixServices.getServices();
        generalAvailability = ZabbixServices.getGeneralAvailability();

        model.addAttribute("title", title);
        model.addAttribute("header", header);
        model.addAttribute("refreshInterval", pageRefreshInterval);
        model.addAttribute("lastUpdated",lastUpdated);
        model.addAttribute("services", services);
        model.addAttribute("generalAvailability", generalAvailability);

        if (!supportUrl.isEmpty()){
            model.addAttribute("supportUrl", supportUrl);
        }
        return "index";
    }
}
