package dev.dobicinaitis.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.dobicinaitis.status.dto.ZabbixAuthRequestDTO;
import dev.dobicinaitis.status.dto.ZabbixAuthResponseDTO;
import dev.dobicinaitis.status.dto.ZabbixServiceDTO;
import dev.dobicinaitis.status.dto.ZabbixServiceRequestDTO;
import dev.dobicinaitis.status.dto.ZabbixServiceResponseDTO;
import java.util.Collections;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@NoArgsConstructor
@Data
public class ZabbixApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZabbixApi.class.getName());
    private static String url;
    private static String user;
    private static String password;
    private static int requestId = 0;
    private static final int requestIdRollover = 10000; // API does not specify max value
    private static String auth;
    private static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Log in request to obtain authentication token, which will be used for all following requests.
     * It appears that the token does not expire, so renewal is not needed.
     * @return
     */
    public static boolean login()  {
        LOGGER.info("Sending login request to Zabbix");
        ZabbixAuthRequestDTO requestBody = new ZabbixAuthRequestDTO(user,password, getNextRequestId());
        LOGGER.debug("Zabbix > " + objectToJsonString(requestBody).replace(password,"REDACTED"));
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<ZabbixAuthRequestDTO> request = new HttpEntity<>(requestBody, getHeaders());
        ResponseEntity<String> response  = restTemplate.postForEntity(url, request, String.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            try {
                LOGGER.debug("Zabbix < " + response.getBody().replaceAll("(.*)(result\":\".*\")(,.*)","$1result\":\"REDACTED\"$3"));
                ZabbixAuthResponseDTO responseDto = objectMapper.readValue(response.getBody(), ZabbixAuthResponseDTO.class);
                auth = responseDto.getResult();
                if (isAuthenticated()){
                    LOGGER.info("Successfully logged into Zabbix and got auth. token");
                } else {
                    LOGGER.error("Login Failed. Response from Zabbix: " + response.getBody());
                }
                return true;
            } catch (JsonProcessingException e) {
                LOGGER.error("Authentication failed. Response from Zabbix: " + response.getBody());
                e.printStackTrace();
            }
        } else {
            LOGGER.error("Authentication request failed. Response code: " + response.getStatusCode());
        }
        return false;
    }

    /**
     * Get list of services with names, statuses and child dependency IDs
     * @param serviceIds list of serviceIDs that will be shown as parents
     * @return list with Zabbix service.get objects
     */
    public static List<ZabbixServiceDTO> getServices(String[] serviceIds){
        ZabbixServiceRequestDTO requestBody = new ZabbixServiceRequestDTO(auth, getNextRequestId(), serviceIds);
        LOGGER.debug("Zabbix > " + objectToJsonString(requestBody).replaceAll("(.*)(auth\":\".*\")(,.*)","$1auth\":\"REDACTED\"$3"));
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<ZabbixServiceRequestDTO> request = new HttpEntity<>(requestBody, getHeaders());
        ResponseEntity<String> response  = restTemplate.postForEntity(url, request, String.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            LOGGER.debug("Zabbix < " + response.getBody());
            try {
                return objectMapper.readValue(response.getBody(),ZabbixServiceResponseDTO.class).getServices();
            } catch (JsonProcessingException e) {
                LOGGER.error("Service object mapping failed. Response from Zabbix: " + response.getBody());
                e.printStackTrace();
            }
        } else {
            LOGGER.error("Get service request failed. Response code: " + response.getStatusCode());
        }
        return List.of();
    }

    private static int getNextRequestId(){
        if (requestId > requestIdRollover){
            requestId = 0;
        }
        requestId+=1;
        return requestId;
    }

    private static HttpHeaders getHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }

    public static boolean isAuthenticated(){
        if (auth != null){
            return true;
        }
        return false;
    }

    @SneakyThrows
    public static String objectToJsonString(Object object){
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

    @Value("${zabbix.api.url}")
    public void setUrl(String url) { this.url = url; }

    @Value("${zabbix.user}")
    public void setUser(String user) { this.user = user; }

    @Value("${zabbix.password}")
    public void setPassword(String password) { this.password = password; }
}

