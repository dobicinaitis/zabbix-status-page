package dev.dobicinaitis.status;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZabbixStatusPageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZabbixStatusPageApplication.class, args);
        ZabbixApi.login();
        ZabbixServices.updateServices(); // initial update
    }

}
