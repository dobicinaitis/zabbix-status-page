package dev.dobicinaitis.status;

import dev.dobicinaitis.status.dto.EGeneralAvailability;
import dev.dobicinaitis.status.dto.EServiceStatus;
import dev.dobicinaitis.status.dto.ServiceDTO;
import dev.dobicinaitis.status.dto.ZabbixServiceDTO;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableScheduling
public class ZabbixServices {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZabbixServices.class.getName());
    private static String[] parentServiceIDs;
    private static Map<ServiceDTO,List<ServiceDTO>> services = new LinkedHashMap<>();
    private static String dateFormat;
    private static String lastUpdated;
    private static EGeneralAvailability generalAvailability = EGeneralAvailability.OK;

    /**
     * Update service list and their statuses
     */
    @Scheduled(cron = "${zabbix.service.update.cron.expression}")
    public static void updateServices(){
        if (ZabbixApi.isAuthenticated()){
            LOGGER.info("Updating services");
            Map<ServiceDTO,List<ServiceDTO>> updatedServices =  new LinkedHashMap<>();
            Set<String> dependencyIDs = new LinkedHashSet<>();
            Map<String, ServiceDTO> dependencies = new LinkedHashMap<>();

            LOGGER.debug("Getting services list");
            List<ZabbixServiceDTO> zabbixServiceDTOs = ZabbixApi.getServices(parentServiceIDs);

            // extract unique child dependency IDs
            zabbixServiceDTOs.forEach(
                    parentService ->  parentService.getDependencies().forEach(
                            dependency -> dependencyIDs.add(dependency.getServiceId())
                    )
            );

            LOGGER.debug("Getting child services");
            // initial service response lacks names and statuses for dependencies,
            // therefore we have to make another round trip to get them
            ZabbixApi.getServices(dependencyIDs.toArray(String[]::new)).forEach(
                    dependency -> dependencies.put(
                            dependency.getServiceId()
                            ,new ServiceDTO(dependency.getName(), zabbixStatusToEnum(dependency.getStatus()))
                    )
            );

            // populate services list
            zabbixServiceDTOs.forEach(
                    parentService -> updatedServices.put(
                            new ServiceDTO(
                                    parentService.getName(),
                                    zabbixStatusToEnum(parentService.getStatus())
                            ),
                            parentService.getDependencies()
                                    .stream()
                                    .map(dependency -> dependencies.get(dependency.getServiceId()))
                                    .collect(Collectors.toList())
                    )
            );

            services = updatedServices;
            LocalDateTime now = LocalDateTime.now();
            lastUpdated =  now.format(DateTimeFormatter.ofPattern(dateFormat));
            LOGGER.debug("Latest services list: " + services);
            updateGeneralAvailability();
            LOGGER.debug("General availability: " + generalAvailability.getMessage());
        }
    }

    public static String getLastUpdated(){
        return lastUpdated;
    }

    public static Map<ServiceDTO, List<ServiceDTO>> getServices() {
        return services;
    }

    public static EGeneralAvailability getGeneralAvailability(){
        return generalAvailability;
    }

    /**
     * Show in general banner that some services are unavailable if any has statuses DISASTER, HIGH,
     * having problems if its AVERAGE and operational if severities are lower than that.
     */
    public static void updateGeneralAvailability(){
        EGeneralAvailability status = services.entrySet().stream()
                .filter(service -> service.getKey().getStatus().equals(EServiceStatus.DISASTER)
                                || service.getKey().getStatus().equals(EServiceStatus.HIGH)
                )
                .findFirst()
                .map(s -> EGeneralAvailability.NOK)
                .orElse(EGeneralAvailability.OK);

        if (status.equals(EGeneralAvailability.OK)){
            status = services.entrySet().stream()
                    .filter(service -> service.getKey().getStatus().equals(EServiceStatus.AVERAGE)
                    )
                    .findFirst()
                    .map(s -> EGeneralAvailability.PARTIAL)
                    .orElse(EGeneralAvailability.OK);
        }
        generalAvailability = status;
    }

    private static EServiceStatus zabbixStatusToEnum(String status){
        switch (status){
            case "0" :
                return EServiceStatus.OK;
            case "1" :
                return EServiceStatus.INFORMATION;
            case "2" :
                return EServiceStatus.WARNING;
            case "3" :
                return EServiceStatus.AVERAGE;
            case "4" :
                return EServiceStatus.HIGH;
            case "5" :
                return EServiceStatus.DISASTER;
        }
        return EServiceStatus.OK;
    }

    @Value("${zabbix.services}")
    public void setServiceIds(String parentServiceIDs) {
        this.parentServiceIDs = parentServiceIDs.split(",");
    }

    @Value("${site.date.format:yyyy.MM.dd HH:mm}")
    public void setDateFormat(String dateFormat){
        this.dateFormat = dateFormat;
    }
}
